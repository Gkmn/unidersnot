﻿using MvcProjesi.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity.ModelConfiguration.Configuration;
using System.Data.Entity.ModelConfiguration.Conventions;
using Microsoft.Owin;

namespace MvcProjesi.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/
        public ActionResult Index()
        {
            return View();
        }

        //Son 5 makalenin ana sayfaya yükleneceği Action
        public ActionResult TumUrunler()
        {
            //Veritabanından yeni bir nesne oluşturuyoruz.
            MvcProjesiContext db = new MvcProjesiContext();

            //Veritabanından sorgulamayı Linq ile yapıyoruz.
            List<Urun> UrunListe = db.Uruns.OrderByDescending(i => i.Adi).Take(5).ToList();

            //Normal içeriklerde View döndürürken, burada ise PartialView döndürüyoruz.
            //Ayrıca urunListe nesnesini de View'de kullanacağımız şekilde model olarak aktarıyoruz.
            return PartialView(UrunListe);
        }     

       
/*

        public ActionResult EtiketinMakaleleri(int etiketId)
        {
            MvcProjesiContext db = new MvcProjesiContext();
            var geciciListe = (from i in db.Etikets where i.EtiketId == etiketId select i.Makales).ToList();

            //Burada veri içiçe liste halinde geldiği için, içerideki listeyi [0] indexi ile alıp gönderiyoruz.
            return View(geciciListe[0]);
        }

        public ActionResult MakaleDetay(int makaleId)
        {
            MvcProjesiContext db = new MvcProjesiContext();

            //Burada verilen id numarasına göre seçili makaleyi alıyoruz.
            Makale makale = (from i in db.Makales where i.MakaleId == makaleId select i).SingleOrDefault();
            return View(makale);
        }
        public ActionResult YorumMakalesi(int yorumId)
        {
            MvcProjesiContext db = new MvcProjesiContext();

            //Burada verilen yorumId numarasına göre ait olduğu makaleyi alıyoruz.
            Makale makale = (from i in db.Yorums where i.YorumId==yorumId select i.Makale).SingleOrDefault();
            return View(makale);
        }

        */


        // GET: Forms
        public ActionResult Forms()
        {
            return View();
        }
    }
}



