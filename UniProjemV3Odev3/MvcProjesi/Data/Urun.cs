﻿using Microsoft.Owin;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MvcProjesi.Data
{
    public class Urun
    {
        public int UrunID { get; set; }

        [Required(ErrorMessage = "Lütfen yorumunuzu giriniz.")]
        public string Adi { get; set; }

        [Required(ErrorMessage = "Lütfen yorumunuzu giriniz.")]
        public string Ders { get; set; }

        [Required(ErrorMessage = "Lütfen yorumunuzu giriniz.")]
        public string Bolum { get; set; }

        [Required(ErrorMessage = "Lütfen yorumunuzu giriniz.")]
        [DataType(DataType.Text, ErrorMessage ="Lütfen Açıklama Belirtiniz.")]
        public string Aciklama { get; set; }




        //Her ürün, yalnızca bir üyeye ait olabilir. Bu yüzden, tek bir üyeye bağlıyoruz. 
        //Dikkat edileceği üzere veri türü (burada aynı zamanda sınıf) olarak Makale yazılıyor.
        //Her ürün, yalnızca bir üniye ait olabilir.
        public virtual Universite Universite { get; set; }

        
        public virtual Uye Uye { get; set; }
    }
}