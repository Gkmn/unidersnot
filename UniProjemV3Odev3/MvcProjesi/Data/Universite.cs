﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity.ModelConfiguration.Configuration;
using System.Data.Entity.ModelConfiguration.Conventions;
using Microsoft.Owin;

namespace MvcProjesi.Data
{
    public class Universite
    {
        public int UniversitID { get; set; }

        [Required(ErrorMessage = "Lütfen Üniversite Adını giriniz.")]
        [StringLength(150, ErrorMessage = "Etiketin içeriği 50 karakterden uzun olamaz.")]
        public string Icerik { get; set; }

        //Aynı üni, birden çok ürün de kullanılıyor olabilir.
        public virtual List<Urun> Uruns { get; set; }
    }
}