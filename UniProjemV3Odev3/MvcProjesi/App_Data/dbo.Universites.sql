﻿CREATE TABLE [dbo].[Universites] (
    [UniversiteID] INT           IDENTITY (1, 1) NOT NULL,
    [Icerik]   NVARCHAR (150) NOT NULL,
    CONSTRAINT [PK_dbo.Universites] PRIMARY KEY CLUSTERED ([UniversiteID] ASC)
);