﻿//Veritabanı context sınıfımızı referans veriyoruz
using MvcProjesi.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity.ModelConfiguration.Configuration;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace MvcProjesi
{
    public class MvcApplication : System.Web.HttpApplication
    {
        //Uygulama ilk başlatıldığında, buradaki metod çalışacak.
        protected void Application_Start()
        {
            //Burada veritabanı sınıfımızdan, bir nesne oluşturuyoruz. using kullanmamızın sebebi,
            //db nesnesinin işi bittiğinde, silinmesini ve hafızada yer tutmamasını sağlamak.
            using (MvcProjesiContext db = new MvcProjesiContext())
            {
                //Bu metod, eğer veritabanımız oluşturulmamış ise, oluşturulmasını sağlıyor.
                db.Database.CreateIfNotExists();

                //Veritabanındaki Urunların, üyelerin ve Universitelerin adetini alıyoruz.

                int UrunAdet = (from i in db.Uruns select i).Count();
                int uyeAdet = (from i in db.Uyes select i).Count();
                int UniversiteAdet = (from i in db.Universites select i).Count();

                //Veritabanına, sürekli aynı Urunların eklenmemesi için                
                //Ayrıca sistemde en az 1 üye olduğunu da onaylıyoruz.
                //Bununla birlikte en az 10 adet Universite olduğunu da onaylıyoruz.
                //
                if (UrunAdet < 5 || uyeAdet < 1 || UniversiteAdet < 10)
                {
                    //Bir tane örnek üye oluşturuyoruz.
                    Uye uye = new Uye() { Ad = "iron", Soyad = "iron", EPosta = "iron@deneme.com", ResimYol = "", UyeOlmaTarih = DateTime.Now, WebSite = "http://www.cumhuriyet.com", Sifre = "dondurma" };

                    db.Uyes.Add(uye);

                    //ürünleri oluşturuyoruz. Ayrıca ürünlerin, yukarıda oluşturduğumuz kullanıcı 
                    //tarafından oluşturulduğunu gösteriyoruz.

                    //ürünleri eklemek için komutumuzu veriyoruz.
                    //SaveChanges() komutu gelene kadar veritabanına kayıt yapılmayacak.


                    //Urunlarımızı oluşturuyoruz. Ayrıca Urunların, yukarıda oluşturduğumuz kullanıcı 
                    //tarafından oluşturulduğunu gösteriyor, ayrıca makalelerimize de bağlıyoruz.
                    Urun Urun1 = new Urun() { Adi = "Urun 1 ", Ders = "Web Prog.", Bolum = "Bil. Müh.", Aciklama = "aaa", Uye = uye };
                    Urun Urun2 = new Urun() { Adi = "Urun 2 ", Ders = "Web Prog.", Bolum = "Bil. Müh.", Aciklama = "aaa", Uye = uye };
                    Urun Urun3 = new Urun() { Adi = "Urun 3 ", Ders = "Web Prog.", Bolum = "Bil. Müh.", Aciklama = "aaa", Uye = uye };
                    Urun Urun4 = new Urun() { Adi = "Urun 4 ", Ders = "Web Prog.", Bolum = "Bil. Müh.", Aciklama = "aaa", Uye = uye };
                    Urun Urun5 = new Urun() { Adi = "Urun 5 ", Ders = "Web Prog.", Bolum = "Bil. Müh.", Aciklama = "aaa", Uye = uye };
                    Urun Urun6 = new Urun() { Adi = "Urun 6 ", Ders = "Web Prog.", Bolum = "Bil. Müh.", Aciklama = "aaa", Uye = uye };

                    //Urunları eklemek için komutumuzu veriyoruz.
                    //SaveChanges() komutu gelene kadar veritabanına kayıt yapılmayacak.

                    db.Uruns.Add(Urun1);
                    db.Uruns.Add(Urun2);
                    db.Uruns.Add(Urun3);
                    db.Uruns.Add(Urun4);
                    db.Uruns.Add(Urun5);
                    db.Uruns.Add(Urun6);

                    //Universitelerimizi oluşturuyoruz. Ayrıca Universiteleri, kullanıldığı ürünlerle de bağlıyoruz.
                    Universite Universite1 = new Universite() { Icerik = "Sakarya Üniversite", Uruns = new List<Urun>() { Urun1 } };
                    Universite Universite2 = new Universite() { Icerik = "ODTÜ", Uruns = new List<Urun>() { Urun1, Urun2 } };
                    Universite Universite3 = new Universite() { Icerik = "İtü", Uruns = new List<Urun>() { Urun4 } };
                    Universite Universite4 = new Universite() { Icerik = "Boğaziçi Üniversite", Uruns = new List<Urun>() { Urun5 } };
                    Universite Universite5 = new Universite() { Icerik = "Gazi Üniversite", Uruns = new List<Urun>() { Urun3 } };
                    Universite Universite6 = new Universite() { Icerik = "Anadolu Üniversite", Uruns = new List<Urun>() { Urun6, Urun2 } };
                    Universite Universite7 = new Universite() { Icerik = "Marmara Üniversite", Uruns = new List<Urun>() { Urun1, Urun2, Urun3, Urun4, Urun5, Urun6 } };
                    Universite Universite8 = new Universite() { Icerik = "Özyeğin Üniversite", Uruns = new List<Urun>() { Urun5 } };

                    //Urun urun7 = new Urun() { Universites = new List<Universite>() { Universite1 }, Aciklama = "" }

                    //Universiteleri eklemek için komutumuzu veriyoruz.
                    //SaveChanges() komutu gelene kadar veritabanına kayıt yapılmayacak.
                    db.Universites.Add(Universite1);
                    db.Universites.Add(Universite2);
                    db.Universites.Add(Universite3);
                    db.Universites.Add(Universite4);
                    db.Universites.Add(Universite5);
                    db.Universites.Add(Universite6);
                    db.Universites.Add(Universite7);
                    db.Universites.Add(Universite8);

                    //Son olarak da yaptığımız eklemelerin, veritabanına yansıtılmasını
                    //sağlamak için kaydet komutu veriyoruz.
                }
                db.SaveChanges();

            }
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
   }
 }
